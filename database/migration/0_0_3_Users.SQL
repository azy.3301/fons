create table users (
    id int generated always as identity primary key,
    user_name text not null,
    first_name text not null,
    last_name text not null,
    mail text not null,
    password text not null,
    is_deleted boolean default false
);

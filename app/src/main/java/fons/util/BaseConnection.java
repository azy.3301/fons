package fons.util;

import fons.config.ConnectionDB;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * The BaseConnection class is used to manage databases through other classes
 * @author AZY
 */
public class BaseConnection {

    /**
     * executeSelect method
     * receives data from the database and writes to the fields of the class
     * @param query - request
     * @param model - lambda
     * @return array of an instance of the ArrayList class
     */
    protected static <T> ArrayList<T> executeSelect(String query, ModelBuilder<T> model){
        ArrayList<T> newObject = new ArrayList<T>();
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                newObject.add(model.build(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newObject;
    }

    /**
     * execution of the selection method by id
     * @param query - request
     * @param model - lambda
     * @return an instance of the class
     */
    protected static <T> T executeSelectId(String query, ModelBuilder<T> model){
        return executeSelect(query, model).get(0);
    }

    /**
     * method execute
     * sets a request for an operation in the database
     * @param query - request
     */
    protected static void execute(String query){
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * method executeAdd
     * sets a request to add to the database
     * @param query - request
     */
    protected static void executeAdd(String query){
        execute(query);
    }

    /**
     * method executeUpdate
     * * sets an update request in the database
     * @param query - request
     */
    protected static void executeUpdate(String query){
        execute(query);
    }

    /**
     * executeDeleted method
     * sets a deletion request in the database
     * @param query - request
     */
    protected static void executeDeleted(String query){
        execute(query);
    }
}
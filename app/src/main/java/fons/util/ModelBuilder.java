package fons.util;

import java.sql.ResultSet;

/**
 * Generalized interface for lambda
 * @author AZY
 */
public interface ModelBuilder<T> {
     T build(ResultSet resultSet);
}

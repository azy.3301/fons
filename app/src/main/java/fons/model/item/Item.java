package fons.model.item;

import fons.util.BaseConnection;

/**
 * Abstract class Item is used to combine all secondary entities
 * and extends the BaseConnection class to be able to work with the database
 * @author DanielUsov
 * @version 1.1
 */
public abstract class Item extends BaseConnection {

    /** @value the field stores the header for different secondary entities*/
    protected String header;

    /** @value field stores status for various secondary entities*/
    protected boolean isDeleted;

    /**
     * method getHeader
     * @return header for secondary entities
     */
    public String getHeader(){
        return header;
    }

    /**
     * method setHeader
     * @param header - header for secondary entities
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * method isDeleted
     * @return status for various secondary entities
     */
    public boolean isDeleted() {
        return isDeleted;
    }

    /**
     * method setHeader
     * @param deleted - status for various secondary entities
     */
    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}

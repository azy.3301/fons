package fons.model.item;

import fons.model.User;
import fons.util.BaseConnection;
import fons.util.ModelBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * The Card class is used to establish a connection with the database
 * and data storage
 * @author DanielUsov
 * @version 1.5
 */
public class Card extends Item{

    /** @value the field stores the id of the card */
    private int id;

    /** @value the field stores the columnId of the card */
    private int columnId;

    /** @value the field stores the header of the card */
    private String description;

    /** @value field stores card users */
    private ArrayList<User> users;

    /** @value the field stores the expirationDate of the card */
    private Date expirationDate;

    /** @value a field that stores a query string to receive all cards from the database */
    private static final String QUERY_SELECT_ALL_FIELDS = "select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards";

    /**
     * Card constructor
     * @param id - id cards
     * @param columnId - id columns
     * @param header - header cards
     * @param description - description cards
     * @param users - list of users cards
     * @param expirationDate - expirationDate cards
     * @param isDeleted - isDeleted cards
     */
    public Card(int id, int columnId, String header, String description, ArrayList<User> users, Date expirationDate, boolean isDeleted) {
        this.id = id;
        this.columnId = columnId;
        this.header = header;
        this.description = description;
        this.users = users;
        this.expirationDate = expirationDate;
        this.isDeleted = isDeleted;
    }

    /**
     * empty Card constructor
     */
    public Card() {
    }

    /**
     * method getId
     * @return card id
     */
    public int getId() {
        return id;
    }

    /**
     * method setID
     * @param id - card id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method getColumn
     * @return column id
     */
    public int getColumnId() {
        return columnId;
    }

    /**
     * method setColumn
     * @param columnId - column id
     */
    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    /**
     * method getDescription
     * @return description cards
     */
    public String getDescription() {
        return description;
    }

    /**
     * method setColumn
     * @param description - description cards
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * method getUsers
     * @return ArrayList users
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    /**
     * method setUsers
     * @param users - ArrayList users
     */
    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    /**
     * method getExpirationDate
     * @return card expirationDate
     */
    public String getDate() {
        return String.valueOf(expirationDate);
    }

    /**
     * method getExpirationDate
     * @return card expirationDate
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * method setExpirationDate
     * @param expirationDate - expirationDate for the card
     */
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * lambda result
     * creates and populates a Card instance
     */
    static final ModelBuilder<Card> result  = (resultSet) -> {
        Card card = new Card();
        try{
            card.setId(resultSet.getInt("id"));
            card.setColumnId(resultSet.getInt("column_id"));
            card.setHeader(resultSet.getString("header"));
            card.setDescription(resultSet.getString("description"));
            card.setExpirationDate(resultSet.getDate("expiration_date"));
            card.setDeleted(resultSet.getBoolean("is_deleted"));
        }
        catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        return card;
    };

    /**
     * method executeSelect
     * @param query - query string to the database to get a set of records through select
     * @return ArrayList holding classes Card
     */
    public static ArrayList<Card> executeSelect(String query){
        ArrayList<Card> cards = BaseConnection.executeSelect(query, result);
        return cards;
    }

    /**
     * method executeSelectById
     * @param query - database query string to get a single record through select by id
     * @return Card
     */
    private static Card executeSelectById(String query){
        Card card = BaseConnection.executeSelectId(query, result);
        return card;
    }

    /**
     * method getAll
     * @return ArrayList holding classes Card
     */
    public static ArrayList<Card> getAll(){
        return executeSelect(QUERY_SELECT_ALL_FIELDS);
    }

    /**
     * method getCardByID
     * @param id - id cards
     * @return Card
     */
    public static Card getCardByID(int id){
        return executeSelectById(QUERY_SELECT_ALL_FIELDS + " where cards.id = '" + id + "'");
    }

    /**
     * method create
     * @param card - an instance of the card class
     */
    public static void create(Card card) {
        BaseConnection.executeAdd("insert into cards (column_id, header, description, expiration_date) values ('" + card.getColumnId() + "', '" + card.getHeader() + "', '" + card.getDescription() + "', '" + card.getExpirationDate() + "')");
    }

    /**
     * method update
     * @param card - an instance of the card class
     */
    public static void update(Card card){
        BaseConnection.executeUpdate("update cards set column_id = " + card.getColumnId() + ", header = '" + card.getHeader() + "', description = '" + card.getDescription() + "', expiration_date = '" + card.getExpirationDate() + "' where id =  '" + card.getId() + "'");
    }

    /**
     * method softDelete
     * @param card - an instance of the card class
     */
    public static void softDelete(Card card){
        BaseConnection.executeUpdate("update cards set is_deleted = true where id = " + card.getId() + " ");
    }

    /**
     * method hardDelete
     * @param card - an instance of the card class
     */
    public static void hardDelete(Card card){
        BaseConnection.executeDeleted("delete from cards where id = " + card.getId() + "");
    }

    /**
     * method updateValues
     */
    public void updateValues() {
        update(this);
    }
}

package fons.model.item;

import fons.model.User;
import fons.util.BaseConnection;
import fons.util.ModelBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Board class is used to establish a connection with the database and data storage
 * @author AZY
 * @version 1.0
 */
public class Board extends Item {

    /** @value field to store id workspace */
    private int id;

    /** @value stores whiteboards related to a specific workspace */
    protected Workspace workspace;

    /** @value stores whiteboards related to a specific workspace */
    protected int workspaceId;


    /**
     * Board constructor
     * @param id - id Board
     * @param header - header Board
     * @param workspaceId - workspace id
     * @param isDeleted - isDeleted workspace
     */
    public Board(int id, String header, int workspaceId, boolean isDeleted){
        this.id = id;
        this.header = header;
        this.workspaceId = workspaceId;
        this.isDeleted = isDeleted;
    }

    /** Empty Board constructor */
    public Board(){}

    /**
     * method getId
     * @return Board id
     */
    public int getId() {
        return id;
    }

    /**
     * method setID
     * @param id - card id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method getWorkspaceId
     * @return Workspace id
     */
    public int getWorkspaceId() {
        return workspaceId;
    }

    /**
     * method setWorkspaceId
     * @param id - Workspace id
     */
    public void setWorkspaceId(int id) {
        this.workspaceId = id;
    }

    /**
     * lambda result
     * creates and populates a Board instance
     */
    public static final ModelBuilder<Board> buildModel = (resultSet) -> {
        Board board = new Board();
        try {
            board.setId(resultSet.getInt("id"));
            board.setHeader(resultSet.getString("header"));
            board.setWorkspaceId(resultSet.getInt("workspace_id"));
            board.setDeleted(resultSet.getBoolean("is_deleted"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return board;
    };

    /** Method executeSelect
     *  get class list Workspace
     * @param query - query
     * @return return class list Board
     */
    public static ArrayList<Board> executeSelect(String query){
        return BaseConnection.executeSelect(query, buildModel);
    }

    /**
     * method executeSelectById
     * @param query - database query string to get a single record through select by id
     * @return Card
     */
    private static Board executeSelectById(String query) {
        return BaseConnection.executeSelectId(query, buildModel);
    }

    /**
     * method update
     * the current workspace that we are updating in the database
     */
    public void update() {
        update(this);
    }

    /** @value field stores sql for getAll */
    static final String sqlGetAll = "SELECT id, header, workspace_id, is_deleted FROM boards";

    /**
     * static method getAll
     * @return array class instance ArrayList keeping in itself Board
     */
    public static ArrayList<Board> getAll() {
        return executeSelect(sqlGetAll);
    }

    /**
     * static method getByID
     * @param id - id of the received board
     * @return array class instance ArrayList keeping in itself Workspace
     */
    public static Board getByID(int id){
        return executeSelectById("SELECT id, header, workspace_id, is_deleted FROM boards where boards.id = '" + id + "'");
    }

    public static ArrayList<Column> getColumn(Board board){
        return Column.executeSelect("SELECT id, header, board_id, is_deleted FROM columns WHERE board_id = '" + board.getId() + "'");
    }

    /**
     * static method create
     * @param board - board which we add to the database
     */
    public static void create(Board board) {
        BaseConnection.executeAdd("INSERT into boards (header, workspace_id) values ('" + board.getHeader() + "', '" + board.getWorkspaceId() + "')");
    }

    /**
     * static method update
     * @param board - board which we update in the database
     */
    public static void update(Board board) {
        BaseConnection.executeUpdate("UPDATE boards SET header = '" + board.getHeader() + "', workspace_id = '" + board.getWorkspaceId() + "', is_deleted = '" + board.isDeleted() + "' WHERE id = " + board.id + "");
    }

    /**
     * static method softDelete
     * @param board - an instance of the board class
     */
    public static void softDelete(Board board) {
        board.setDeleted(true);
        BaseConnection.executeUpdate("with column_ids as (UPDATE boards set is_deleted = true where id = " + board.getId() + " returning (select id from columns where board_id = " + board.getId() + "))," +
                "card_ids as (UPDATE columns set is_deleted = true where id in (select id from column_ids) returning (select id from cards where column_id = (select id from column_ids)))" +
                "update cards set is_deleted = true where id in (select id from card_ids);");
    }

    /**
     * static method hardDelete
     * @param board - an instance of the board class
     */
    public static void hardDelete(Board board) {
        BaseConnection.executeDeleted("DELETE FROM boards WHERE id='" + board.getId() + "'");
    }

    /**
     * static method hardDelete
     * @param id - an instance of the board class
     */
    public static void hardDelete(String id) {
        BaseConnection.executeDeleted("DELETE FROM boards WHERE id='" + id + "'");
    }
}

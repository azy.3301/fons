package fons.model.item;

import fons.model.User;
import fons.util.BaseConnection;
import fons.util.ModelBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Workspace class is used to establish a connection with the database and data storage
 * @author AZY
 * @version 1.0
 */
public class Workspace extends Item {

    /** @value field to store id workspace */
    private int id;

    /** @value field stores workspace users */
    private ArrayList<User> users;

    /** @value stores whiteboards related to a specific workspace */
    protected ArrayList<Board> boards;

    /**
     * Workspace constructor
     * @param id - id workspace
     * @param header - header workspace
     * @param isDeleted - isDeleted workspace
     */
    public Workspace(int id, String header, boolean isDeleted) {
        this.id = id;
        this.header = header;
        this.isDeleted = isDeleted;
    }

    /** Empty Workspace constructor */
    public Workspace(){}

    /**
     * method setID
     * @param id - card id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method setID
     * @return Workspace id
     */
    public int getId() {
        return id;
    }

    /**
     * method addBoard
     * @param board - board
     */
    public void addBoard(Board board) {
        this.boards.add(board);
    }

    /**
     * method removeBoard
     * @return Boards
     */
    public ArrayList<Board> getBoards(){
        return this.boards;
    }

    /**
     * method removeBoard
     * @param board - board
     */
    public void removeBoard(Board board){
        this.boards.remove(board);
    }

    /**
     * method addBoard
     * @param user - user
     */
    public void addUser(User user) {
        this.users.add(user);
    }

    /**
     * method removeBoard
     * @return Boards
     */
    public ArrayList<User> getUsers(){
        return this.users;
    }

    /**
     * method removeUser
     * @param User - User
     */
    public void removeUser(User User){
            users.remove(User);
    }

    /**
     * lambda result
     * creates and populates a Workspace instance
     */
    public static final ModelBuilder<Workspace> buildModel = (resultSet) -> {
        Workspace workspace = new Workspace();
        try {
            workspace.setId(resultSet.getInt("id"));
            workspace.setHeader(resultSet.getString("header"));
            workspace.setDeleted(resultSet.getBoolean("is_deleted"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return workspace;
    };

    /** Method executeSelect
     *  get class list Workspace
     * @param query - query
     * @return return class list Workspace
     */
    private static ArrayList<Workspace> executeSelect(String query){
        ArrayList<Workspace> workspace = BaseConnection.executeSelect(query, buildModel);
        return workspace;
    }

    /**
     * method executeSelectById
     * @param query - database query string to get a single record through select by id
     * @return Card
     */
    private static Workspace executeSelectById(String query){
        Workspace workspace = BaseConnection.executeSelectId(query, buildModel);
        return workspace;
    }

    /**
     * method update
     * the current workspace that we are updating in the database
     */
    public void update() {
        update(this);
    }

    /** @value field stores sql for getAll */
    static final String sqlGetAll = "SELECT id, header, is_deleted FROM workspaces";

    /**
     * static method getAll
     * @return array class instance ArrayList keeping in itself Workspace
     */
    public static ArrayList<Workspace> getAll() {
        return executeSelect(sqlGetAll);
    }

    /**
     * static method getByID
     * @param id - id of the received Workspace
     * @return array class instance ArrayList keeping in itself Workspace
     */
    public static Workspace getByID(int id){
        return executeSelectById("SELECT id, header, is_deleted FROM workspaces where workspaces.id = '" + id + "'");
    }

    public static ArrayList<Board> getBoards(Workspace workspace){
        return Board.executeSelect("SELECT id, header, workspace_id, is_deleted FROM boards WHERE workspace_id = '" + workspace.getId() + "'");
    }

    /**
     * static method create
     * @param workspace - workspace which we add to the database
     */
    public static void create(Workspace workspace) {
        BaseConnection.executeAdd("INSERT into workspaces (header) values ('" + workspace.getHeader() + "')");
    }

    /**
     * static method create
     * @param header - workspace which we add to the database
     */
    public static boolean create(String header) {
        if ((header != null) || (header.equals(""))) {
            BaseConnection.executeAdd("INSERT into workspaces (header) values ('" + header + "')");
            return true;
        } else {
            return false;
        }
    }

    /**
     * static method update
     * @param workspace - workspace which we update in the database
     */
    public static void update(Workspace workspace) {
        BaseConnection.executeUpdate("UPDATE workspaces SET header = '" + workspace.getHeader() + "', is_deleted = '" + workspace.isDeleted() + "' WHERE id = " + workspace.id + "");
    }

    /**
     * static method softDelete
     * @param workspace - an instance of the workspace class
     */
    public static void softDelete(Workspace workspace) {
        workspace.setDeleted(true);
        BaseConnection.executeUpdate("with board_ids as (UPDATE workspaces set is_deleted = true where id = " + workspace.getId() + " returning (select id from boards where workspace_id = " + workspace.getId() + "))," +
                "column_ids as (UPDATE boards set is_deleted = true where id in (select id from board_ids) returning (select id from columns where board_id = (select id from board_ids)))," +
                "card_ids as (UPDATE columns set is_deleted = true where id in (select id from column_ids) returning (select id from cards where column_id = (select id from column_ids)))" +
                "update cards set is_deleted = true where id in (select id from card_ids);");
    }

    /**
     * static method hardDelete
     * @param workspace - an instance of the workspace class
     */
    public static void hardDelete(Workspace workspace) {
        BaseConnection.executeDeleted("DELETE FROM workspaces WHERE id='" + workspace.getId() + "'");
    }

    /**
     * static method hardDelete
     * @param id - an instance of the workspace class
     */
    public static boolean hardDelete(String id) {
        if ((id != null) || (id.equals(""))) {
            BaseConnection.executeDeleted("DELETE FROM workspaces WHERE id='" + id + "'");
            return true;
        } else {
            return false;
        }
    }
}
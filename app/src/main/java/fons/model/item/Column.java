package fons.model.item;

import fons.util.BaseConnection;
import fons.util.ModelBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The Column class is used to establish a connection with the database
 * and data storage
 * @author DanielUsov
 * @version 1.0
 */
public class Column extends Item{

    private static final String QUERY_SELECT_ALL_FIELDS = "select columns.id, columns.board_id, columns.header, columns.is_deleted from columns";
    /** @value the field stores the id of the column */
    private int id;

    /** @value the field stores the boardId of the column */
    private int boardId;

    /** @value field stores all cards in column */
    private ArrayList<Card> cards;

    /**
     * Column constructor
     * @param id - id cards
     * @param boardId - id boards
     * @param header - header cards
     * @param cards - cards list
     * @param isDeleted - isDeleted cards
     */
    public Column(int id, int boardId, String header, ArrayList<Card> cards, boolean isDeleted) {
        this.id = id;
        this.boardId = boardId;
        this.header = header;
        this.cards = cards;
        this.isDeleted = isDeleted;
    }

    /**
     * empty Card constructor
     */
    public Column() {
    }

    /**
     * method getId
     * @return column id
     */
    public int getId() {
        return id;
    }

    /**
     * method setId
     * @param id - column id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method getBoardId
     * @return board id
     */
    public int getBoardId() {
        return boardId;
    }

    /**
     * method setBoardId
     * @param boardId - board id
     */
    public void setBoardId(int boardId) {
        this.boardId = boardId;
    }

    /**
     * lambda result
     * creates and populates a Column instance
     */
    static final ModelBuilder<Column> result  = (resultSet) -> {
        Column column = new Column();
        try{
            column.setId(resultSet.getInt("id"));
            column.setBoardId(resultSet.getInt("board_id"));
            column.setHeader(resultSet.getString("header"));
            column.setDeleted(resultSet.getBoolean("is_deleted"));
        }
        catch (SQLException e ) {
            System.out.println(e.getMessage());
        }
        return column;
    };

    /**
     * method executeSelect
     * @param query - query string to the database to get a set of records through select
     * @return ArrayList holding classes Column
     */
    public static ArrayList<Column> executeSelect(String query){
        return BaseConnection.executeSelect(query, result);
    }

    /**
     * method executeSelectById
     * @param query - database query string to get a single record through select by id
     * @return Column
     */
    private static Column executeSelectById(String query){
        return BaseConnection.executeSelectId(query, result);
    }

    /**
     * method getAll
     * @return ArrayList holding classes Column
     */
    public static ArrayList<Column> getAll(){
        return executeSelect(QUERY_SELECT_ALL_FIELDS);
    }

    /**
     * method getCardByID
     * @param id - id column
     * @return Column
     */
    public static Column getByID(int id){
        return executeSelectById(QUERY_SELECT_ALL_FIELDS + " where columns.id = '" + id + "'");
    }

    public static ArrayList<Card> getCard(Column column){
        return Card.executeSelect("SELECT id, column_id, header, description, expiration_date, is_deleted FROM cards WHERE column_id = '" + column.getId() + "'");
    }

    /**
     * method create
     * @param column - an instance of the column class
     */
    public static void create(Column column) {
        BaseConnection.executeAdd("insert into columns (board_id, header) values (" + column.getBoardId() + ", '" + column.getHeader() + "')");
    }

    /**
     * method update
     * @param column - an instance of the column class
     */
    public static void update(Column column){
        BaseConnection.executeUpdate("UPDATE columns SET header = '" + column.getHeader() + "', board_id = '" + column.getBoardId() + "' WHERE id =  '" + column.getId() + "'");
    }

    /**
     * method softDelete
     * @param column - an instance of the column class
     */
    public static void softDelete(Column column) {
        column.setDeleted(true);
        BaseConnection.executeUpdate("with card_ids as (UPDATE columns SET is_deleted = true WHERE id = " + column.getId() + " returning (select id from cards where column_id = " + column.getId() + "))" +
                "update cards set is_deleted = true where id in (select id from card_ids);");
    }


    /**
     * method hardDelete
     * @param column - an instance of the column class
     */
    public static void hardDelete(Column column){
        BaseConnection.executeDeleted("delete from columns where id = " + column.getId() + "");
    }

    /**
     * method updateValues
     */
    public void updateValues() {
        update(this);
    }

}

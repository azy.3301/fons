package fons.model;

import fons.util.BaseConnection;
import fons.util.ModelBuilder;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The User class is used to establish a connection with the database and data storage
 * @author AZY and kuzy53
 * @version 1.0
 */
public class User extends BaseConnection{
    /** @value field to store id User */
    private int id;
    /** @value field to store name user */
    private String userName;
    /** @value field to store first name */
    private String firstName;
    /** @value field to store last name */
    private String lastName;
    /** @value field to store user mail */
    private String mail;
    /** @value field to store user password */
    private String password;
    /** @value field to store isDeleted */
    private boolean isDeleted;

    /**
     * User constructor
     * @param id - User id
     * @param userName - User name
     * @param firstName - User firstName
     * @param lastName - User lastName
     * @param mail - User mail
     * @param password - User password
     * @param isDeleted - isDeleted user
     */
    public User(int id, String userName, String firstName, String lastName, String mail, String password, boolean isDeleted) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.password = password;
        this.isDeleted = isDeleted;
    }

    /** User constructor */
    public User(){
    }

    /**
     * method setId
     * @param id - User id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * method getId
     * @return User Id
     */
    public int getId() {
        return this.id;
    }

    /**
     * method setUserName
     * @param userName - User name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * method getUserName
     * @return userName
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * method setFirstName
     * @param firstName - first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * method getFirstName
     * @return first Name
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * method setLastName
     * @param lastName - last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * method getLastName
     * @return last name
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * method setMail
     * @param mail - mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * method getMail
     * @return mail
     */
    public String getMail() {
        return this.mail;
    }

    /**
     * method setPassword
     * @param password - password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * method getPassword
     * @return password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * method setDeleted
     * @param deleted - the user is deleted or not
     */
    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    /**
     * method isDeleted
     * @return boolean isDeleted
     */
    public boolean isDeleted() {
        return isDeleted;
    }


    /**
     * lambda result
     * creates and populates a User instance
     */
    public static ModelBuilder<User> buildModel  = (resultSet) -> {
        User user = new User();
        try{
            user.setId(resultSet.getInt("id"));
            user.setUserName(resultSet.getString("user_name"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setMail(resultSet.getString("mail"));
            user.setPassword(resultSet.getString("password"));
            user.setDeleted(resultSet.getBoolean("is_deleted"));
        }
        catch (SQLException e ) {System.out.println(e.getMessage());}
        return user;
    };

    /** method executeSelect
     *  get class list User
     * @param query - query
     * @return return class list User
     */
    private static ArrayList<User> executeSelect(String query){
        return BaseConnection.executeSelect(query, buildModel);
    }

    /**
     * static method update
     * the current user we are updating in the database
     */
    public void update() {
        update(this);
    }

    /** @value field stores sql for getAll */
    static final String SQL_GET_ALL = "SELECT id, user_name, first_name, last_name, mail, password, is_deleted from users";

    /**
     * static method getAll
     * @return array class instance ArrayList keeping in itself User
     */
    public static ArrayList<User> getAll() {
        return executeSelect(SQL_GET_ALL);
    }

    /**
     * static method getById
     * @param id - user id
     * @return an array element of an instance of the ArrayList class
     */
    public static User getById(int id) {
        return executeSelect("SELECT id, user_name, first_name, last_name, mail, password, is_deleted from users where id = '" + id + "'").get(0);
    }

    /**
     * static method create
     * @param user - User which we add to the database
     */
    public static void create(User user) {
        BaseConnection.executeAdd("INSERT INTO users (user_name, first_name, last_name, mail, password) VALUES ('" + user.userName + "', '" + user.firstName + "', '" + user.lastName + "', '" + user.mail + "', '" + user.password + "')");
    }

    /**
     * static method update
     * updating any record in the user's database
     * @param user - instance of the User class
     */
    public static void update(User user) {
        BaseConnection.executeUpdate("UPDATE users SET user_name = '" + user.userName + "', first_name = '" + user.firstName + "', last_name = '" + user.lastName + "', mail = '" + user.mail + "', password = '" + user.password + "', is_deleted = '" + user.isDeleted()  + "' WHERE id = " + user.id + "");
    }

    /**
     * static method isDelete
     * updating any record in the user's database
     * @param user - instance of the User class
     */
    public static void softDelete(User user) {
        BaseConnection.executeUpdate("UPDATE users SET is_deleted = true WHERE id = " + user.id + "");
    }

    /**
     * static method isDelete
     * Delete a record from the database
     * @param user - instance of the User class
     */
    public static void hardDelete(User user) {
        BaseConnection.executeUpdate("DELETE FROM users WHERE id = " + user.getId() + "");
    }
}

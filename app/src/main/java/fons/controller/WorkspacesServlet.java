package fons.controller;

import fons.model.item.Workspace;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import java.io.IOException;

import jakarta.servlet.annotation.WebServlet;

/**
 * Address servlet class "/workspaces"
 * @author AZY
 */
@WebServlet("/workspaces")
public class WorkspacesServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the workspaces page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        request.getRequestDispatcher("/fons/views/workspaces.jsp").forward(request, response);
    }

    /**
     * POST request processing method creating a Workspace
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            request.setCharacterEncoding("UTF-8");
            String name = request.getParameter("name");
            if (name == null) {
                response.sendError(422);
            } else {
                Workspace.create(name);
                request.getRequestDispatcher("/fons/views/workspaces.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

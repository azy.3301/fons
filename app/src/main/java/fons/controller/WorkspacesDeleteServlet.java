package fons.controller;

import fons.model.item.Workspace;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/workspaces_delete/*"
 * @author AZY
 */
@WebServlet("/workspaces_delete/*")
public class WorkspacesDeleteServlet extends HttpServlet{

    /**
     * POST request processing method removing the workspace
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try{
            Workspace.hardDelete(UtilServlet.getId(request));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally{
            response.sendRedirect(request.getContextPath() + "/workspaces");
        }
    }
}

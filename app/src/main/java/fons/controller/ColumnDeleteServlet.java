package fons.controller;

import fons.model.item.Board;
import fons.model.item.Column;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/column_delete/*"
 * @author AZY
 */
@WebServlet("/column_delete/*")
public class ColumnDeleteServlet extends HttpServlet{

    /**
     * POST request processing method removing the Column
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Column column = Column.getByID(Integer.parseInt(UtilServlet.getId(request)));
        try{
            Column.hardDelete(column);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally{
            response.sendRedirect(request.getContextPath() + "/boards/" + column.getBoardId());
        }
    }
}

package fons.controller;

import fons.model.item.Board;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/board_delete/*"
 * @author AZY
 */
@WebServlet("/board_delete/*")
public class BoardDeleteServlet extends HttpServlet{

    /**
     * POST request processing method removing the board
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Board board = Board.getByID(Integer.parseInt(UtilServlet.getId(request)));
        try{
            Board.hardDelete(board);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally{
            response.sendRedirect(request.getContextPath() + "/workspace/" + board.getWorkspaceId());
        }
    }
}

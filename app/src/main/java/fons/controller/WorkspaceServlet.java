package fons.controller;

import fons.model.item.Board;
import fons.model.item.Workspace;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/workspace/*"
 * @author AZY
 */
@WebServlet("/workspace/*")
public class WorkspaceServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the workspace page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try{
            request.setAttribute("workspace_id", UtilServlet.getId(request));
            request.setAttribute("boards", Workspace.getBoards(Workspace.getByID(Integer.parseInt(UtilServlet.getId(request)))));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        request.getRequestDispatcher("/fons/views/workspace.jsp").forward(request, response);
    }

    /**
     * POST request processing method creating a Board
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            request.setCharacterEncoding("UTF-8");
            int id = Integer.parseInt(UtilServlet.getId(request));
            String name = request.getParameter("name");
            if (name == null) {
                response.sendError(422);
            } else {
                Board board = new Board();
                board.setWorkspaceId(id);
                board.setHeader(name);
                fons.model.item.Board.create(board);
            }
            response.sendRedirect("/fons/workspace/" + id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

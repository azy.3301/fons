package fons.controller;

import fons.model.item.Board;
import fons.model.item.Card;
import fons.model.item.Column;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Address servlet class "/card_edit"
 * @author AZY
 */
@WebServlet("/card_edit/*")
public class CardEditServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the card_edit page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        int id = Integer.parseInt(UtilServlet.getId(request));
        Card card = Card.getCardByID(id);
        request.setAttribute("header", card.getHeader());
        request.setAttribute("id", card.getId());
        request.setAttribute("id_board",Column.getByID(card.getColumnId()).getBoardId());
        request.getRequestDispatcher("/fons/views/card_edit.jsp").forward(request, response);
    }

    /**
     * POST request processing method editing the Card
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Card card = Card.getCardByID(Integer.parseInt(request.getParameter("id")));
        try {
            request.setCharacterEncoding("UTF-8");
            String newName = request.getParameter("name");
            card.setHeader(newName);
            card.setDescription(request.getParameter("description"));
            Card.update(card);
            if (newName == null) {
                response.sendError(422);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            response.sendRedirect(request.getContextPath() + "/boards/" + Column.getByID(card.getColumnId()).getBoardId());
        }
    }
}

package fons.controller;

import fons.model.item.Card;
import fons.model.item.Column;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/card_delete/*"
 * @author AZY
 */
@WebServlet("/card_delete/*")
public class CardDeleteServlet extends HttpServlet{

    /**
     * POST request processing method removing the Card
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Card card = Card.getCardByID(Integer.parseInt(UtilServlet.getId(request)));
        try{
            Card.hardDelete(card);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally{
            response.sendRedirect(request.getContextPath() + "/boards/" + Column.getByID(card.getColumnId()).getBoardId());
        }
    }
}

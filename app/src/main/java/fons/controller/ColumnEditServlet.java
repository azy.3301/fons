package fons.controller;

import fons.model.item.Board;
import fons.model.item.Column;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Address servlet class "/column_edit"
 * @author AZY
 */
@WebServlet("/column_edit/*")
public class ColumnEditServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the columns_edit page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        int id = Integer.parseInt(UtilServlet.getId(request));
        Column column = Column.getByID(id);
        request.setAttribute("header", column.getHeader());
        request.setAttribute("id", column.getId());
        request.setAttribute("id_board",column.getBoardId());
        request.getRequestDispatcher("/fons/views/column_edit.jsp").forward(request, response);
    }

    /**
     * POST request processing method editing the Column
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Column column = Column.getByID(Integer.parseInt(request.getParameter("id")));
        try {
            request.setCharacterEncoding("UTF-8");
            String newName = request.getParameter("name");
            column.setHeader(newName);
            Column.update(column);
            if (newName == null) {
                response.sendError(422);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            response.sendRedirect(request.getContextPath() + "/boards/" + column.getBoardId());
        }
    }
}

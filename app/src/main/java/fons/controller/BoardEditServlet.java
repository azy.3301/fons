package fons.controller;

import fons.model.item.Board;
import fons.model.item.Workspace;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Address servlet class "/board_edit"
 *
 * @author AZY
 */
@WebServlet("/board_edit/*")
public class BoardEditServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the boards_edit page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        int id = Integer.parseInt(UtilServlet.getId(request));
        Board board =  Board.getByID(id);
        request.setAttribute("header", board.getHeader());
        request.setAttribute("id", board.getId());
        request.setAttribute("id_workspace",board.getWorkspaceId());
        request.getRequestDispatcher("/fons/views/board_edit.jsp").forward(request, response);
    }

    /**
     * POST request processing method editing the board
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Board board = Board.getByID(Integer.parseInt(UtilServlet.getId(request)));
        try {
            request.setCharacterEncoding("UTF-8");
            board.setHeader(request.getParameter("header"));
            Board.update(board);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            response.sendRedirect(request.getContextPath() + "/workspace/" + board.getWorkspaceId());
        }
    }
}

package fons.controller;

import fons.model.item.Board;
import fons.model.item.Card;
import fons.model.item.Column;
import fons.model.item.Workspace;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/boards/*"
 *
 * @author AZY
 */
@WebServlet("/boards/*")
public class BoardServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the board page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try{
            int id = Integer.parseInt(UtilServlet.getId(request));
            Board board = Board.getByID(id);
            request.setAttribute("board_id", UtilServlet.getId(request));
            request.setAttribute("workspace_id", board.getWorkspaceId());
            request.setAttribute("columns", Board.getColumn(board));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        request.getRequestDispatcher("/fons/views/board.jsp").forward(request, response);
    }

    /**
     * POST request processing method creating a Column
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            request.setCharacterEncoding("UTF-8");
            String name = request.getParameter("name");
            int id = Integer.parseInt(UtilServlet.getId(request));
            if (name == null) {
                response.sendError(422);
            } else {
                Column column = new Column();
                column.setBoardId(id);
                column.setHeader(name);
                Column.create(column);
            }
            response.sendRedirect("/fons/boards/" + id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

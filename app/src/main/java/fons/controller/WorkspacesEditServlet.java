package fons.controller;

import fons.model.item.Workspace;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/workspaces_edit"
 * @author AZY
 */
@WebServlet("/workspaces_edit/*")
public class WorkspacesEditServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the workspaces_edit page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        int id = Integer.parseInt(UtilServlet.getId(request));
        Workspace workspace = Workspace.getByID(id);
        request.setAttribute("header", workspace.getHeader());
        request.setAttribute("id", workspace.getId());
        request.getRequestDispatcher("/fons/views/workspaces_edit.jsp").forward(request, response);
    }

    /**
     * POST request processing method editing the Workspace
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            int id = Integer.parseInt(UtilServlet.getId(request));
            request.setCharacterEncoding("UTF-8");
            String newName = request.getParameter("header");
            Workspace workspace = Workspace.getByID(id);
            workspace.setHeader(newName);
            Workspace.update(workspace);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            response.sendRedirect(request.getContextPath() + "/workspaces");
        }
    }
}

package fons.controller;

import fons.model.item.Card;
import fons.model.item.Column;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/columns/*"
 * @author AZY
 */
@WebServlet("/columns/*")
public class ColumnServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the board page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try{
            request.setAttribute("board_id", UtilServlet.getId(request));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        request.getRequestDispatcher("/fons/views/board.jsp").forward(request, response);
    }

    /**
     * POST request processing method creating a Card
     * @param request - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            request.setCharacterEncoding("UTF-8");
            String name = request.getParameter("card_name");
            if (name == null) {
                response.sendError(422);
            } else {
                Card card = new Card();
                card.setColumnId(Integer.parseInt(UtilServlet.getId((request))));
                card.setHeader(name);
                card.setDescription("");
                String dateInString = "9999-99-99 99:99:99";
                Date date = UtilServlet.getSDF().parse(dateInString);
                card.setExpirationDate(date);
                card.setUsers(null);
                Card.create(card);
            }
            response.sendRedirect("/fons/boards/" + request.getParameter("post_board_id"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

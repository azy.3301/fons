package fons.controller;

import jakarta.servlet.http.HttpServletRequest;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "UtilServlet"
 * @author AZY
 */
public class UtilServlet {

    /** @value the field stores the SimpleDateFormat */
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

    /**
     * method returns SimpleDateFormat
     * @return returns SimpleDateFormat
     */
    public static SimpleDateFormat getSDF(){
        return sdf;
    }

    /**
     * method of receiving the id from the URL string
     * @param request - an object containing a request received from the client
     * @return returns the id workspace
     */
    public static String getId(HttpServletRequest request){
        Matcher workspaceId = Pattern.compile("(\\d+\\z)").matcher(request.getRequestURI());
        workspaceId.find();
        return workspaceId.group();
    }
}

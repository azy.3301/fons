package fons.controller;

import fons.model.item.Board;
import fons.model.item.Card;
import fons.model.item.Column;
import fons.model.item.Workspace;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Address servlet class "/cards/*"
 * @author AZY
 */
@WebServlet("/cards/*")
public class CardServlet extends HttpServlet{

    /**
     * method of processing GET requests, output of the card page
     * @param request  - an object containing a request received from the client
     * @param response - the object that defines the response to the client
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try{
            int id = Integer.parseInt(UtilServlet.getId(request));
            Card card = Card.getCardByID(id);
            request.setAttribute("card_id", id);
            request.setAttribute("board_id", Column.getByID(card.getColumnId()).getBoardId());
            request.setAttribute("card", card);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        request.getRequestDispatcher("/fons/views/card.jsp").forward(request, response);
    }
}

package fons.config;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * The ConnectionDB class is used to create a database connection
 * @author AZY
 */
public class ConnectionDB {
    
    /** @value field of the Connection object */
    private static Connection connection;

    /**
     * createConnection method
     * creates a database connection
     * @param url - database url
     * @param user - name of the database
     * @param password - Connection password
     * @return field of the Connection object
     */
    public static Connection createConnection(String url, String user, String password) throws SQLException{
        Connection connection = DriverManager.getConnection(url, user, password);
        return connection;
    }

    /**
     * createConnection method
     * checks the status of the connection field
     * @return field of the Connection object
     */
    public static Connection getConnection() throws SQLException{
        if (connection == null){
            connection = createConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "123");
        }
        return connection;
    }
}

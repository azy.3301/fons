<%@ page import="fons.model.item.Workspace" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <%@include file="style.jsp" %>
    <body class="body">
        <header class="header">
            <h2 style="padding: 0"><a class="link" href="/fons/workspaces">Fons</a></h2>
        </header>
        <table>
            <tr>
                <td>Num</td>
                <td>Workspaces</td>
                <td>Workspaces id</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            <%int i = 1;%>
            <%for (Workspace workspace : Workspace.getAll()){%>
            <tr>
                <td><%=i%></td>
                <td>
                    <form method="get" action="/fons/workspace/<%= workspace.getId()%>">
                        <button class="button" type="submit" ><%=workspace.getHeader()%></button>
                    </form>
                </td>
                <td><%=workspace.getId()%></td>
                <td>
                    <form method="get" action="/fons/workspaces_edit/<%=workspace.getId()%>">
                        <button class="button" type="submit">Edit</button>
                    </form>
                </td>
                <td>
                    <form method="post" action="/fons/workspaces_delete/<%= workspace.getId() %>">
                        <button class="button" type="submit">Delete</button>
                    </form>
                </td>
                <%i++;%>
            </tr>
            <%}%>
        </table>
        <form style="padding : 15px" method="post">
            <label for="name">Workspace name: </label>
            <input class="input" id="name" type="text" name="name">
            <input class="submit" type="submit" value="Add">
        </form>
    </body>
</html>
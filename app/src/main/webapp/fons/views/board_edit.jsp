<%@ page import="fons.model.item.Board" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<%@include file="style.jsp" %>
    <body class="body">
        <header class="header">
            <h2 style="padding: 0"><a class="link" href="/fons/workspaces">Fons</a></h2>
        </header>
        <form method="get" action="/fons/workspace/<%=request.getAttribute("id_workspace")%>">
            <button class="button_back" type="submit" >Back</button>
        </form>
        <p>Workspace name: <%=request.getAttribute("header")%></p>
        <form style="padding : 15px" method="post">
            <label for="header">Board new name: </label>
            <input type="text" class="input" id="header" name="header">
            <input type="hidden" id="id" name="id" value="<%=request.getAttribute("id")%>">
            <input class="submit" type="submit" value="Save">
        </form>
    </body>
</html>
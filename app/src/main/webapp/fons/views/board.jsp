<%@ page import="fons.model.item.Board" %>
<%@ page import="fons.model.item.Column" %>
<%@ page import="fons.model.item.Card" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<%@include file="style.jsp" %>
<body class="body">
<header class="header">
    <h2 style="padding: 0"><a class="link" href="/fons/workspaces">Fons</a></h2>
</header>
<form method="get" action="/fons/workspace/<%=request.getAttribute("workspace_id")%>">
    <button class="button_back" type="submit" >Back</button>
</form>
<form style="padding-bottom : 15px" method="post">
    <label for="name">New column: </label>
    <input class="input" id="name" type="text" name="name">
    <input type="hidden" id="board_id" name="board_id" value="<%=request.getAttribute("board_id")%>">
    <input class="submit" type="submit" value="Add">
</form>
<div>
    <%for (Column column :  (ArrayList<Column>) request.getAttribute("columns")){%>
    <div class="table_line">
        <table>
            <tr>
                <th colspan="3"><%=column.getHeader()%></th>
                <th colspan="1">
                    <form method="get" action="/fons/column_edit/<%=column.getId()%>">
                        <button class="button" type="submit">Edit</button>
                    </form>
                </th>
                <th colspan="1">
                    <form method="post" action="/fons/column_delete/<%= column.getId() %>">
                        <button class="button" type="submit" >Delete</button>
                    </form>
                </th>
            </tr>
            <tr>
                <td>Num</td>
                <td>Card</td>
                <td>Card id</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            <%int i = 1;%>
            <%for (Card card : Column.getCard(column)){%>
            <tr>
                <td><%=i%></td>
                <td>
                    <form method="get" action="/fons/cards/<%= card.getId() %>">
                        <button class="button" type="submit" ><%=card.getHeader()%></button>
                    </form>
                </td>
                <td><%=card.getId()%></td>
                <td>
                    <form method="get" action="/fons/card_edit/<%=card.getId()%>">
                        <button class="button" type="submit">Edit</button>
                    </form>
                </td>
                <td>
                    <form method="post" action="/fons/card_delete/<%= card.getId() %>">
                        <button class="button" type="submit">Delete</button>
                    </form>
                </td>
                <%i++;%>
            </tr>
            <%}%>
            <tr>
                <th colspan="5">
                    <form style="padding : 3px" method="post" action="/fons/columns/<%=column.getId()%>">
                        <label for="card_name">New card: </label>
                        <input class="input" id="card_name" type="text" name="card_name">
                        <input type="hidden" id="post_board_id" name="post_board_id" value="<%=request.getAttribute("board_id")%>">
                        <input class="submit" type="submit" value="Add">
                    </form>
                </th>
            </tr>
        </table>
    </div>
    <%}%>
</div>
</body>
</html>

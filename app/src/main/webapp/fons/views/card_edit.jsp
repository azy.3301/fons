<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
    <%@include file="style.jsp" %>
    <body class="body">
        <header class="header">
            <h2 style="padding: 0"><a class="link" href="/fons/workspaces">Fons</a></h2>
        </header>
        <form method="get" action="/fons/boards/<%=request.getAttribute("id_board")%>">
            <button class="button_back" type="submit" >Back</button>
        </form>
        <p>Card name: <%=request.getAttribute("header")%></p>
        <form style="padding : 15px" method="post">
            <label for="name">Card new name: </label>
            <input type="text" class="input" id="name" name="name"><br><br>
            <input type="hidden" id="id" name="id" value="<%=request.getAttribute("id")%>">
            <label for="description">Card new description: </label>
            <input type="text" class="input" id="description" name="description"><br><br>
            <input class="submit" type="submit" value="Save">
        </form>
    </body>
</html>
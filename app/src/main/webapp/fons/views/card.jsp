<%@ page import="fons.model.item.Card" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <%@include file="style.jsp" %>
    <body class="body">
        <header class="header">
            <h2 style="padding: 0"><a class="link" href="/fons/workspaces">Fons</a></h2>
        </header>
        <form method="get" action="/fons/boards/<%=request.getAttribute("board_id")%>">
            <button class="button_back" type="submit" >Back</button>
        </form>
        <table>
            <%Card card = (Card) request.getAttribute("card");%>
            <tr>
                <td>id_card</td>
                <td><%=card.getId()%></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><%=card.getHeader()%></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><%=card.getDescription()%></td>
            </tr>
            <tr>
                <td>Data</td>
                <td><%=card.getDate()%></td>
            </tr>
        </table>
    </body>
</html>
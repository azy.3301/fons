<%@ page import="fons.model.item.Workspace" %>
<%@ page import="fons.model.item.Board" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <%@include file="style.jsp" %>
    <body class="body">
        <header class="header">
            <h2 style="padding: 0"><a class="link" href="/fons/workspaces">Fons</a></h2>
        </header>
        <form method="get" action="/fons/workspaces">
            <button class="button_back" type="submit" >Back</button>
        </form>
        <br><br>
        <table>
            <tr>
                <td>Num</td>
                <td>Board</td>
                <td>Board id</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            <%int i = 1;%>
            <%for (Board board : (ArrayList<Board>) request.getAttribute("boards")){%>
            <tr>
                <td><%=i%></td>
                <td>
                    <form method="get" action="/fons/boards/<%= board.getId() %>">
                        <button class="button" type="submit" value="<%=board.getId()%>"><%=board.getHeader()%></button>
                    </form>
                </td>
                <td><%=board.getId()%></td>
                <td>
                    <form method="get" action="/fons/board_edit/<%=board.getId()%>">
                        <button class="button" type="submit">Edit</button>
                    </form>
                </td>
                <td>
                    <form method="post" action="/fons/board_delete/<%= board.getId() %>">
                        <button class="button" type="submit">Delete</button>
                    </form>
                </td>
                <%i++;%>
            </tr>
            <%}%>
        </table>
        <form style="padding : 15px" method="post">
            <label for="name">Board name: </label>
            <input class="input" id="name" type="text" name="name">
            <input type="hidden" id="workspace_id" name="workspace_id" value="<%=request.getAttribute("workspace_id")%>">
            <input class="submit" type="submit" value="Add">
        </form>
    </body>
</html>
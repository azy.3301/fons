<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
    <%@include file="style.jsp" %>
    <body class="body">
        <header class="header">
            <h2 style="padding: 0"><a class="link" href="/fons/workspaces">Fons</a></h2>
        </header>
        <form method="get" action="/fons/boards/<%=request.getAttribute("id_board")%>">
            <button class="button_back" type="submit" >Back</button>
        </form>
        <p>Column name: <%=request.getAttribute("header")%></p>
        <form style="padding : 15px" method="post">
            <label for="name">Column new name: </label>
            <input type="text" class="input" id="name" name="name">
            <input type="hidden" id="id" name="id" value="<%=request.getAttribute("id")%>">
            <input class="submit" type="submit" value="Save">
        </form>
    </body>
</html>
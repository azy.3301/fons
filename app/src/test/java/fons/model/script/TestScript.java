package fons.model.script;

import fons.config.ConnectionDB;
import fons.util.ModelBuilder;
import org.junit.jupiter.api.DisplayName;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@DisplayName("TestScript \uD83D\uDC3C️")
public class TestScript {

    protected void executeClean() {
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            String[] executeDelete = {"DELETE from users_cards;",
                    "DELETE from users_workspaces;",
                    "DELETE from users_boards;",
                    "DELETE from cards;",
                    "DELETE from columns;",
                    "DELETE from users;",
                    "DELETE from boards;",
                      "DELETE from workspaces;"};
            for (String query : executeDelete) {
                statement.execute(query);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void execute(String query) {
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected boolean executeCheck(String query) {
        boolean answer = false;
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                answer = resultSet.getBoolean("bool");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(answer);
        if (!answer){
            return false;
        } else {
            return answer;
        }
    }

    protected static <T> ArrayList<T> executeSelect(String query, ModelBuilder<T> model){
        ArrayList<T> newObject = new ArrayList<T>();
        try {
            Statement statement = ConnectionDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                newObject.add(model.build(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newObject;
    }
}

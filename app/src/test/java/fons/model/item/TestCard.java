package fons.model.item;

import fons.model.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TestCard \uD83D\uDC3C️")
public class TestCard extends TestScript {

    @BeforeEach
    public void beforeEach() throws ParseException {
        executeClean();
        for (int i = 0; i <= 3; i++) {
            Workspace workspace = new Workspace();
            workspace.setHeader("workspace_" + i);
            Workspace.create(workspace);
        }
        int i = 0;
        for (Workspace workspace : Workspace.getAll()) {
            Board board = new Board();
            board.setHeader("board_" + i);
            board.setWorkspaceId(workspace.getId());
            Board.create(board);
            i++;
        }
        i = 0;
        for (Board board: Board.getAll()) {
            Column column = new Column();
            column.setHeader("column_" + i);
            column.setBoardId(board.getId());
            Column.create(column);
            i++;
        }
        i = 0;
        for (Column column: Column.getAll()) {
            Card card = new Card();
            card.setHeader("card_" + i);
            card.setColumnId(column.getId());
            card.setDescription("my_card_" + i);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
            String dateInString = "2023-10-02 10:23:54";
            Date date = sdf.parse(dateInString);
            card.setExpirationDate(date);
            Card.create(card);
            i++;
        }
    }

    @Test
    public void getAll() {
        ArrayList<Card> cards = Card.getAll();
        assertEquals(cards.size(), 4);
    }

    @Test
    public void getByID() {
        Card card = executeSelect("select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards where cards.header = 'card_1'", Card.result).get(0);
        Card secondCard = Card.getCardByID(card.getId());
        assertEquals(secondCard.getId(), card.getId());
    }

    @Test
    public void create() throws ParseException {
        Column column = executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'column_1'", Column.result).get(0);
        assertEquals(Card.getAll().size(), 4);
        for (int i = 0; i <= 5; i++) {
            Card card = new Card();
            card.setHeader("new_card_" + i);
            card.setColumnId(column.getId());
            card.setDescription("my_new_card_" + i);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
            Date date = sdf.parse("2023-10-02 10:23:54");
            card.setExpirationDate(date);
            Card.create(card);
        }
        assertEquals(Card.getAll().size(), 10);
    }

    @Test
    public void update() {
        Card card = executeSelect("select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards where cards.header = 'card_1'", Card.result).get(0);
        card.setHeader("newHeader");
        int newColumnId = Column.getAll().get(2).getId();
        card.setColumnId(newColumnId);
        Card.update(card);
        assertEquals(executeSelect("select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards where cards.header = 'newHeader'", Card.result).get(0).getHeader(), "newHeader");
        assertEquals(executeSelect("select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards where cards.header = 'newHeader'", Card.result).get(0).getColumnId(), newColumnId);
    }

    @Test
    public void softDelete() {
        Card oldCard = executeSelect("select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards where cards.header = 'card_1'", Card.result).get(0);
        Card.softDelete(oldCard);
        Card newCard = executeSelect("select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards where cards.header = 'card_1'", Card.result).get(0);
        assertTrue(newCard.isDeleted());
    }

    @Test
    public void hardDelete() {
        Card card = executeSelect("select cards.id, cards.column_id, cards.header, cards.description, cards.expiration_date, cards.is_deleted from cards where cards.header = 'card_1'", Card.result).get(0);
        Card.hardDelete(card);
        assertFalse(executeCheck("select true from cards where header = 'card_1'"));
    }

}

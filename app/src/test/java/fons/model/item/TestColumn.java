package fons.model.item;

import fons.model.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TestColumn \uD83D\uDC3C️")
public class TestColumn extends TestScript{
    @BeforeEach
    public void beforeEach() {
        executeClean();
        for (int i = 0; i <= 3; i++) {
            Workspace workspace = new Workspace();
            workspace.setHeader("workspace_" + i);
            Workspace.create(workspace);
        }
        int i = 0;
        for (Workspace workspace : Workspace.getAll()) {
            Board board = new Board();
            board.setHeader("board_" + i);
            board.setWorkspaceId(workspace.getId());
            Board.create(board);
            i++;
        }
        i = 0;
        for (Board board: Board.getAll()) {
            Column column = new Column();
            column.setHeader("column_" + i);
            column.setBoardId(board.getId());
            Column.create(column);
            i++;
        }
    }

    @Test
    public void getAll() {
        ArrayList<Column> columns = Column.getAll();
        assertEquals(columns.size(), 4);
    }

    @Test
    public void getByID() {
        Column column = executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'column_1'", Column.result).get(0);
        Column secondColumn = Column.getByID(column.getId());
        assertEquals(secondColumn.getId(), column.getId());
    }

    @Test
    public void create() {
        Board board = executeSelect("select boards.id, boards.header, boards.is_deleted from boards where boards.header = 'board_1'", Board.buildModel).get(0);
        assertEquals(Column.getAll().size(), 4);
        for (int i = 0; i <= 2; i++) {
            Column column = new Column();
            column.setHeader("new_board_" + i);
            column.setBoardId(board.getId());
            Column.create(column);
        }
        assertEquals(Column.getAll().size(), 7);
    }

    @Test
    public void update() {
        Column getColumn = executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'column_1'", Column.result).get(0);
        getColumn.setHeader("newHeader");
        int newBoardId = Board.getAll().get(2).getId();
        getColumn.setBoardId(newBoardId);
        Column.update(getColumn);
        assertEquals(executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'newHeader'", Column.result).get(0).getHeader(), "newHeader");
        assertEquals(executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'newHeader'", Column.result).get(0).getBoardId(), newBoardId);
    }

    @Test
    public void softDelete() {
        Column getColumn = executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'column_1'", Column.result).get(0);
        Column.softDelete(getColumn);
        Column getNewColumn = executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'column_1'", Column.result).get(0);
        assertTrue(getNewColumn.isDeleted());
    }

    @Test
    public void hardDelete() {
        Column getColumn = executeSelect("select columns.id, columns.board_id, columns.header, columns.is_deleted from columns where columns.header = 'column_1'", Column.result).get(0);
        Column.hardDelete(getColumn);
        assertFalse(executeCheck("select true from columns where header = 'column_1'"));
    }
}

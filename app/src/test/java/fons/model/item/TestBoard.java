package fons.model.item;

import fons.model.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TestBoard \uD83D\uDC3C️")
public class TestBoard extends TestScript {

    @BeforeEach
    public void beforeEach() {
        executeClean();
        for (int i = 0; i <= 2; i++) {
            Workspace workspace = new Workspace();
            workspace.setHeader("workspace_" + i);
            Workspace.create(workspace);
        }
        int i = 0;
        for (Workspace workspace : Workspace.getAll()) {
            Board board = new Board();
            board.setHeader("board_" + i);
            board.setWorkspaceId(workspace.getId());
            Board.create(board);
            i++;
        }
    }

    @Test
    public void getAll() {
        ArrayList<Board> boards = Board.getAll();
        assertEquals(boards.size(), 3);
    }

    @Test
    public void getByID() {
        Board board = executeSelect("SELECT id, header, workspace_id, is_deleted FROM boards where boards.header = 'board_1'", Board.buildModel).get(0);
        Board getBoard = Board.getByID(board.getId());
        assertEquals(getBoard.getId(), board.getId());
    }

    @Test
    public void create() {
        Workspace workspace = executeSelect("SELECT id, header, is_deleted FROM workspaces where workspaces.header = 'workspace_1'", Workspace.buildModel).get(0);
        assertEquals(Board.getAll().size(), 3);
        for (int i = 0; i <= 2; i++) {
            Board board = new Board();
            board.setHeader("new_workspace_" + i);
            board.setWorkspaceId(workspace.getId());
            Board.create(board);
        }
        assertEquals(Board.getAll().size(), 6);
    }

    @Test
    public void update() {
        Board getBoard = executeSelect("SELECT id, header, workspace_id, is_deleted FROM boards where boards.header = 'board_1'", Board.buildModel).get(0);
        getBoard.setHeader("newHeader");
        ArrayList<Board> boards = Board.getAll();
        int newWorkspaceId = Workspace.getAll().get(2).getId();
        getBoard.setWorkspaceId(newWorkspaceId);
        Board.update(getBoard);
        assertEquals(executeSelect("SELECT id, header, workspace_id, is_deleted FROM boards where boards.header = 'newHeader'", Board.buildModel).get(0).getHeader(), "newHeader");
        assertEquals(executeSelect("SELECT id, header, workspace_id, is_deleted FROM boards where boards.header = 'newHeader'", Board.buildModel).get(0).getWorkspaceId(), newWorkspaceId);
    }

    @Test
    public void softDelete() {
        Board getBoard = executeSelect("SELECT id, workspace_id, header, is_deleted FROM boards where boards.header = 'board_1'", Board.buildModel).get(0);
        Board.softDelete(getBoard);
        Board getNewBoard = executeSelect("SELECT id, workspace_id, header, is_deleted FROM boards where boards.header = 'board_1'", Board.buildModel).get(0);
        assertTrue(getNewBoard.isDeleted());
    }

    @Test
    public void hardDelete() {
        Board getBoard = executeSelect("SELECT id, workspace_id, header, is_deleted FROM boards where boards.header = 'board_2'", Board.buildModel).get(0);
        Board.hardDelete(getBoard);
        boolean check = executeCheck("SELECT true FROM boards where header = 'board_2'");
        assertFalse(check);
    }
}

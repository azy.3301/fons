package fons.model.item;

import fons.model.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TestWorkspace \uD83D\uDC3C️")
public class TestWorkspace extends TestScript {

    @BeforeEach
    public void beforeEach() {
        executeClean();
        for (int i = 0; i <= 2; i++) {
            Workspace workspace = new Workspace();
            workspace.setHeader("workspace_" + i);
            Workspace.create(workspace);
        }
    }

    @Test
    public void getAll() {
        ArrayList<Workspace> workspaces = Workspace.getAll();
        assertEquals(workspaces.size(), 3);
    }

    @Test
    public void getByID() {
        Workspace workspace = executeSelect("SELECT id, header, is_deleted FROM workspaces where workspaces.header = 'workspace_1'", Workspace.buildModel).get(0);
        Workspace getWorkspace = Workspace.getByID(workspace.getId());
        assertEquals(getWorkspace.getId(), workspace.getId());
    }

    @Test
    public void create() {
        executeClean();
        assertEquals(Workspace.getAll().size(), 0);
        for (int i = 0; i <= 5; i++) {
            Workspace workspace = new Workspace();
            workspace.setHeader("new_workspace_" + i);
            Workspace.create(workspace);
        }
        assertEquals(Workspace.getAll().size(), 6);
    }

    @Test
    public void update() {
        Workspace getWorkspace = executeSelect("SELECT id, header, is_deleted FROM workspaces where workspaces.header = 'workspace_1'", Workspace.buildModel).get(0);
        getWorkspace.setHeader("newHeader");
        Workspace.update(getWorkspace);
        assertEquals(executeSelect("SELECT id, header, is_deleted FROM workspaces where workspaces.header = 'newHeader'", Workspace.buildModel).get(0).getHeader(), "newHeader");
    }

    @Test
    public void softDelete() {
        Workspace getWorkspace = executeSelect("SELECT id, header, is_deleted FROM workspaces where workspaces.header = 'workspace_1'", Workspace.buildModel).get(0);
        Workspace.softDelete(getWorkspace);
        Workspace getNewWorkspace = executeSelect("SELECT id, header, is_deleted FROM workspaces where workspaces.header = 'workspace_1'", Workspace.buildModel).get(0);
        assertTrue(getNewWorkspace.isDeleted());
    }

    @Test
    public void hardDelete() {
        Workspace getWorkspace = executeSelect("SELECT id, header, is_deleted FROM workspaces where workspaces.header = 'workspace_2'", Workspace.buildModel).get(0);
        Workspace.hardDelete(getWorkspace);
        boolean check = executeCheck("SELECT true FROM workspaces where header = 'workspace_2'");
        assertFalse(check);
    }
}

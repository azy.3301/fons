package fons.model;

import fons.model.script.TestScript;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TestBoard \uD83D\uDC3C️")
public class UserTest extends TestScript{


    @BeforeEach
    public void beforeEach() {
        executeClean();
        for (int i = 0; i <= 2; i++) {
            User user = new User();
            user.setUserName("UserName_" + i);
            user.setFirstName("FirstName_" + i);
            user.setLastName("LastName_" + i);
            user.setMail("Mail_" + i);
            user.setPassword("Password_" + i);
            User.create(user);
        }
    }

    @Test
    public void getAll(){
        assertEquals(User.getAll().size(), 3);
    }

    @Test
    public void getByID(){
        User user = executeSelect("SELECT id, user_name, first_name, last_name, mail, password, is_deleted FROM users where users.user_name = 'UserName_1'", User.buildModel).get(0);
        User newUser = User.getById(user.getId());
        assertEquals(newUser.getId(), user.getId());
    }

    @Test
    public void create(){
        assertEquals(User.getAll().size(), 3);
        for (int i = 0; i <= 2; i++){
            User user = new User();
            user.setUserName("UserName_" + i);
            user.setFirstName("FirstName_" + i);
            user.setLastName("LastName_" + i);
            user.setMail("Mail_" + i);
            user.setPassword("Password_" + i);
            User.create(user);
        }
        assertEquals(User.getAll().size(), 6);
    }

    @Test
    public void update(){
        User user = executeSelect("SELECT id, user_name, first_name, last_name, mail, password, is_deleted FROM users where user_name = 'UserName_1'", User.buildModel).get(0);
        user.setUserName("UserName_999");
        User.update(user);
        assertEquals(User.getById(user.getId()).getUserName(), "UserName_999");
    }

    @Test
    public void softDelete() {
        User user = executeSelect("SELECT id, user_name, first_name, last_name, mail, password, is_deleted FROM users where user_name = 'UserName_1'", User.buildModel).get(0);
        User.softDelete(user);
        User deleteBoard = executeSelect("SELECT id, user_name, first_name, last_name, mail, password, is_deleted FROM users where user_name = 'UserName_1'", User.buildModel).get(0);
        assertTrue(deleteBoard.isDeleted());
    }

    @Test
    public void hardDelete() {
        User user = executeSelect("SELECT id, user_name, first_name, last_name, mail, password, is_deleted FROM users where user_name = 'UserName_1'", User.buildModel).get(0);
        User.hardDelete(user);
        boolean check = executeCheck("SELECT true FROM users where user_name = 'UserName_1'");
        assertFalse(check);
    }
}
